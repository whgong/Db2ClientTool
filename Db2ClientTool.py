#! pythonw

import os
import wx
from pages.mainFrame import MainFrame
from cfg import cfg
from dbOper import db2oper

def firstRun(catalogPath):
    catalog = dict()
    tmp = db2oper.ConnStr('rake_db(rake)', '127.0.0.1', '50000', 'TCPIP', \
                          'rake_db', 'rake', 'qqq111=')
    catalog[tmp.connName] = tmp.baseTypeObj()
    
    tmp = db2oper.ConnStr('yyzy64(yyzyusr)','10.10.11.64', '50001', 'TCPIP', \
                          'yyzy', 'yyzyusr', 'yyzyusr')
    catalog[tmp.connName] = tmp.baseTypeObj()
    cfg.saveCFG(catalogPath, catalog)
    
    pass

if __name__ == "__main__": 
    
    catalogPath = os.path.join('cfg','catalog.json')
    searchSolutionPath = os.path.join('cfg','searchSolution.json')
     
    #firstRun(catalogPath)
    appContext = dict()
    appContext["catalogs"] = cfg.readCFG(catalogPath)
    appContext["searchSolution"] = cfg.readCFG(searchSolutionPath)
    appContext["cfg"] = dict()
    appContext["cfg"]["result_grid_count"] = 6 
     
    app = wx.App()
     
    frame = MainFrame(None,appContext)
    frame.Maximize()
    frame.Show()
     
    app.MainLoop()
    pass
