'''
Created on 2015-4-10
@author: whgong
'''

class DBAction(object):
    def do(self, objName, params, context):
        raise Exception("must override this method:action");
    pass

class DBSelectAction(DBAction):
    def do(self, objName, params, context):
        sql = "select * from " + objName
        mFrame = context['mainFrame']
        mFrame.addScriptPage(scriptStr = sql)
        pass
    pass

class DBExtractDLLAction(DBAction):
    def do(self, objName, params, context): 
#         sql = "CALL SYSPROC.ADMIN_CMD('describe table dim.t_dim_yyzy_yylb show detail')"
        print "ExtractDLL"
        pass
    pass

class DBTableDescribeAction(DBAction):
    def do(self, objName, params, context): 
        sql = "CALL SYSPROC.ADMIN_CMD('describe table " + objName + " show detail')"
        mFrame = context['mainFrame']
        mFrame.addScriptPage(scriptStr = sql)
        pass
    pass

###################################################################
class DBObject(object):
    '''
    classdocs
    '''
    def __init__(self, objName):
        '''
        Constructor
        '''
        object.__init__(self)
        self.__objName = objName
        self.__actions = dict()
    
    def __del__(self):
        del self.actions
        del self.objName
        object.__del__(self)
           
    @property
    def objectName(self):return self.__objName
    @property
    def actions(self):return self.__actions
    
    def addAction(self, actionName, action):
        self.actions[actionName] = action
        pass
    
    def doAction(self, actionName, params, context):
        action = self.actions[actionName]
        action.do(self.objectName, params, context)

###################################################################
class DBObjectTable(DBObject):
    def __init__(self, name):
        DBObject.__init__(self, name)
        self.addAction("Select", DBSelectAction())
        self.addAction("ExtractDLL", DBExtractDLLAction())
        self.addAction("Describe", DBTableDescribeAction())
        
class DBObjectView(DBObject):
    def __init__(self, name):
        DBObject.__init__(self, name)
        self.addAction("Select", DBSelectAction())
        self.addAction("ExtractDLL", DBExtractDLLAction())
        
class DBObjectNickname(DBObject):
    def __init__(self, name):
        DBObject.__init__(self, name)
        self.addAction("Select", DBSelectAction())
        self.addAction("ExtractDLL", DBExtractDLLAction())
    