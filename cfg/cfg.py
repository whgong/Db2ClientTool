#!D:\Program_Files\Python27\python.exe

import json
from dbOper import db2oper

def readCFG(path):
    try:
        cfgfile = open(path,"r")
    except IOError:
        cfgfile = open(path, "w+")

    try:
        cfgs = json.load(cfgfile)
    except ValueError:
        cfgs = dict()
    return cfgs
    pass

def saveCFG(path, cfgs):
    cfgfile = open(path, "w+")

    json.dump(cfgs, cfgfile)
    pass


if __name__ == "__main__":
    cfgs = dict()
    catalog = list()
    tmp = db2oper.ConnStr('127.0.0.1', '50000', 'TCPIP', \
                          'rake_db', 'rake', 'qqq111=')
    catalog.append(tmp.baseTypeObj())
    
    tmp = db2oper.ConnStr('10.10.11.64', '50001', 'TCPIP', \
                          'rake_db', 'rake', 'qqq111=')
    catalog.append(tmp.baseTypeObj())
##    cfgs = [{"host":"127.0.0.1", "port":"50000", "dbname":"rake_db"},\
##            {"host":"localhost", "port":"50000", "dbname":"rake_db"},\
##            {"host":"10.10.11.64", "port":"60001", "dbname":"yyzy"}]
    cfgs["catalog"] = catalog
    saveCFG("catalog.json",cfgs)

    cfgs2 = readCFG("catalog.json")
    print cfgs2
    print dir(cfgs2)
    print cfgs2.__class__()
    print cfgs2[0]["host"]
    pass
