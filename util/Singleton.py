'''
Created on 2014-12-1

@author: rake
'''

def Singleton(cls, *args, **kw):
    '''
    Singleton
    '''
    instance = {}
    def _singleton():
        if cls not in instance:
            instance[cls] = cls(*args, **kw)
        return instance[cls]
    return _singleton()
