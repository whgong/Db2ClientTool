#!D:\Program_Files\Python27\python.exe

import wx
import wx.xrc as xrc
import os, sys
from DCT_SCP import SqlScriptPanel
from DCT_OSP import ObjectSearchPanel

try:
    dirName = os.path.dirname(os.path.abspath(__file__))
except:
    dirName = os.path.dirname(os.path.abspath(sys.argv[0]))

sys.path.append(os.path.split(dirName)[0])

try:
    import agw.flatnotebook as FNB
except ImportError: # if it's not there locally, try the wxPython lib.
    import wx.lib.agw.flatnotebook as FNB

class MainBook(FNB.FlatNotebook):
    '''main book'''
    def __init__(self,parent, appContext):
        self.__appContext = appContext
        bookStyle = FNB.FNB_NODRAG| \
                    FNB.FNB_X_ON_TAB| \
                    FNB.FNB_NO_X_BUTTON| \
                    FNB.FNB_TABS_BORDER_SIMPLE
        FNB.FlatNotebook.__init__(self, parent, wx.ID_ANY, agwStyle=bookStyle)

        ##self.Freeze()
        self.addMainPage()
        self.addScriptPage()
        ##self.Thaw()
        pass
    
    @property
    def appContext(self):return self.__appContext
    
    def addMainPage(self):
        '''add a welcome page'''
        res = xrc.EmptyXmlResource()
        res.Load('xrc_file/mainpage.xrc')
        panel = res.LoadPanel(self, "panel_mainpage")
        self.AddPage(panel, "welcome")
        pass
    
    def addScriptPage(self, filename="untitled", scriptStr = None):
        '''add a script page'''
        panel = SqlScriptPanel(self, self.appContext, scriptStr)
        self.AddPage(panel, filename)
        pass
    
    def addObjSearchPage(self):
        '''add a object search page'''
        panel = ObjectSearchPanel(self, self.appContext)
        self.AddPage(panel, "Search")
        pass
    pass
