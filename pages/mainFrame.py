import wx
import wx.xrc as xrc
from pages.DCT_PNB import MainBook
from dbOper import db2oper as db2
from exception import exceptionHandler as ehandler
import thread

class MainFrame(wx.Frame):
    def __init__(self, parent, appContext):
##        wx.Frame.__init__(self, parent)
        # Two stage creation (see http://wiki.wxpython.org/index.cgi/TwoStageCreation)
        pre = wx.PreFrame()
        res = xrc.EmptyXmlResource()
        res.Load('xrc_file/mainFrame.xrc')
        res.LoadOnFrame(pre, parent, "frame_main")
        self.PostCreate(pre)

        self.__appContext = appContext

        mainSizer = wx.BoxSizer(wx.VERTICAL)
        self.SetSizer(mainSizer)
        self.appContext["mainFrame"] = self
        self.book = MainBook(self, self.appContext)
        mainSizer.Add(self.book, 1, wx.ALL | wx.EXPAND)
        mainSizer.Layout()
        
        self.catalogChoice = xrc.XRCCTRL(self, 'choicCatalog', wx.Choice)
        self.toolbar = xrc.XRCCTRL(self, 'toolbar1', wx.ToolBar)
        
        for key in self.appContext['catalogs'].iterkeys():
            self.catalogChoice.Append(key)
        if len(self.appContext['catalogs'])>0 : 
            self.catalogChoice.SetSelection(0)
        self.catalogChoice.Append("--add new--")
        self.catalogChoice.Append("--tmp add--")
        
        self.Bind(wx.EVT_CHOICE, self.Evt_Choice, self.catalogChoice)

        self.bindEvent()
        
        pass
    
    @property
    def appContext(self):return self.__appContext

    def bindEvent(self):
        self.Bind(wx.EVT_TOOL, self.evt_tool_exec, id=xrc.XRCID('tool_execute'))
        self.Bind(wx.EVT_TOOL, self.evt_tool_new, id=xrc.XRCID('tool_new'))
        self.Bind(wx.EVT_TOOL, self.evt_tool_search, id=xrc.XRCID('tool_search'))
        pass

    def getSelectedCatalog(self):
        cv = self.catalogChoice.GetString(self.catalogChoice.GetSelection()) 
        try:
            res = self.appContext['catalogs'][cv]
        except:
            raise StandardError("undefined catalog!!!please choose a illegal catalog")
        return res
    
    def addScriptPage(self, title="untitled", scriptStr=None):
        self.book.addScriptPage(title, scriptStr)
        self.book.SetSelection(self.book.GetPageCount()-1)
        pass
    
    @staticmethod
    @ehandler.ExceptHandler
    def evt_tool_exec_run(page,catalog,execBtn,toolbar1):
        try:
            toolbar1.EnableTool(execBtn, False)
            script = page.getScript()
            seperator = page.getSeperator()
            autoCommit = page.getAutoCommitStatus()
            res = db2.execScriptsDb2(catalog, script, \
                                 seperator, autoCommit)
            
            page.packResultSet(res)
        except Exception as e:
            raise e
        finally:
            toolbar1.EnableTool(execBtn, True)
        pass
    
#     @ehandler.ExceptHandler
    def evt_tool_exec(self, event):
        print "click tool_exec"
        execBtnId = xrc.XRCID('toolbar1', -1)
        self.toolbar = xrc.XRCCTRL(self, 'toolbar1', wx.ToolBar)
        p1 = self.book.GetCurrentPage()
        catalog = self.getSelectedCatalog()
        thread.start_new_thread(MainFrame.evt_tool_exec_run, (p1,catalog,execBtnId,self.toolbar))
        pass
    
    @ehandler.ExceptHandler
    def evt_tool_new(self, event) : 
        print "click tool_new"
        self.addScriptPage()
        pass

    @ehandler.ExceptHandler
    def evt_tool_search(self, event) : 
        print "click tool_search"
        self.book.addObjSearchPage()
        self.book.SetSelection(self.book.GetPageCount()-1);
        pass
    
    @ehandler.ExceptHandler
    def Evt_Choice(self, event):
        
        pass

    
    pass
