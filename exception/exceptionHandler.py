'''
Created on 2014-12-1

@author: rake
'''

import wx
import traceback

def ExceptHandler(func):
    '''
    ExceptHandler use descriptor
    '''
    def _handler_func(*args, **kw):
        try:
            res = func(*args, **kw)
        except Exception as e:
            e_str = traceback.format_exc()
            print e_str;
            diag = wx.MessageDialog(None, str(e), 'Error', wx.OK | wx.ICON_ERROR)
            diag.ShowModal()
            raise Exception("stop current operation cause of exception")
            pass
        return res
    return _handler_func